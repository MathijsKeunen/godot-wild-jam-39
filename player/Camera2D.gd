# Copyright (C) 2021  Mathijs Keunen

# This file is part of galactic monsters

# Galactic Monsters is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Galactic Monsters is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Camera2D

const zoom_speed := Vector2(0.1, 0.1)
const min_zoom := 0.2
const max_zoom := 10.0

func _input(e: InputEvent) -> void:
	var event := e as InputEventMouseButton
	if event:
		if event.button_index == BUTTON_WHEEL_UP and zoom.x > min_zoom:
			zoom -= zoom_speed
		elif event.button_index == BUTTON_WHEEL_DOWN and zoom.x < max_zoom:
			zoom += zoom_speed
