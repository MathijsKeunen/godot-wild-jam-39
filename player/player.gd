# Copyright (C) 2021  Mathijs Keunen

# This file is part of galactic monsters

# Galactic Monsters is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Galactic Monsters is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends RigidBody2D
class_name Player


signal died

export var ANGULAR_MULTIPLIER := 5.0

export var WALK_SPEED := 200.0
export var WALK_ACCELERATION := 500
export var FLOAT_SPEED := 50.0
export var FLOAT_ACCELERATION := 150

export var JUMP_SPEED := 100.0

export var BULLET_SPEED := 500.0
export var SHOOT_TIMEOUT := 2.0

export var FLIP_OFFSET := -160.0

const GROUND_DETECTION_LENGTH := 5.0
const COLLISION_ANGLE := PI/4

onready var sprite := $AnimatedSprite as AnimatedSprite
onready var bullet_position_left := $bullet_position_left as Position2D
onready var bullet_position_right := $bullet_position_right as Position2D

onready var bullet_scene := preload("res://player/bullet.tscn")
onready var bullet_destination := $"../bullets"
onready var timer := $Timer as Timer

var dead := false


func _integrate_forces(state: Physics2DDirectBodyState) -> void:
	if dead:
		return
	var gravity := state.total_gravity
	var player_rotation := state.transform.get_rotation()
	var down := Vector2.DOWN.rotated(player_rotation)
	
	var torque := down.angle_to(gravity)
	state.angular_velocity = torque * ANGULAR_MULTIPLIER
	
	var speed_vec := _get_move_direction(down)
	if speed_vec.length_squared() > 0:
		var velocity_vector := state.linear_velocity.project(speed_vec)
		var velocity := velocity_vector.length()
		if velocity_vector.angle_to(speed_vec) != 0:
			velocity *= -1
		var max_speed := WALK_SPEED if _is_on_ground(down) else FLOAT_SPEED
		var acceleration := WALK_ACCELERATION if _is_on_ground(down) else FLOAT_ACCELERATION
		
		if velocity < max_speed:
			state.linear_velocity += acceleration * state.step * speed_vec
	
	if _is_on_ground(down):
		if speed_vec.length_squared() > 0:
			sprite.play("run")
		else:
			sprite.play("idle")
		if _did_just_jump():
			state.apply_impulse(Vector2.ZERO, down.rotated(PI) * JUMP_SPEED)
	else:
		sprite.play("jump")


func _did_just_jump() -> bool:
	return Input.is_action_just_pressed("ui_up")


func _is_on_ground(down: Vector2) -> bool:
	var result := Physics2DTestMotionResult.new()
	if test_motion(down * GROUND_DETECTION_LENGTH, true, 0.08, result):
		return result.collision_normal.angle_to(-down) < COLLISION_ANGLE
	return false


func _get_move_direction(down: Vector2) -> Vector2:
	var input_direction := _get_input_direction()
	if input_direction == -1:
		_flip(true)
	elif input_direction == 1:
		_flip(false)
	if input_direction == 0:
		return Vector2.ZERO
	return down.rotated(-input_direction * PI/2)


func _flip(flip: bool) -> void:
	sprite.flip_h = flip
	sprite.offset = Vector2(FLIP_OFFSET if flip else 0.0, 0.0)


func _get_input_direction() -> int:
	return int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))


func _shoot() -> void:
	if timer.time_left == 0.0:
		$AudioStreamPlayer2D.play()
		var flip := sprite.flip_h
		var spawn_position := bullet_position_left.position if flip else bullet_position_right.position
		var bullet := bullet_scene.instance() as RigidBody2D
		bullet.position = to_global(spawn_position)
		bullet_destination.add_child(bullet)
		var impulse := (Vector2.LEFT if flip else Vector2.RIGHT).rotated(global_rotation) * BULLET_SPEED
		bullet.apply_central_impulse(impulse)
		timer.start(SHOOT_TIMEOUT)


func _unhandled_input(event: InputEvent) -> void:
	if not dead and event.is_action_pressed("shoot"):
		_shoot()


func _die() -> void:
	$AudioStreamPlayer2D2.play()
	dead = true
	sprite.play("hit")
	yield(sprite, "animation_finished")
	emit_signal("died")
#	queue_free()


func _on_player_body_entered(body: Node) -> void:
	if not dead and (body.is_in_group("enemy") or body.is_in_group("bullet")):
		_die()
