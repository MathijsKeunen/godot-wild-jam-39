# Copyright (C) 2021  Mathijs Keunen

# This file is part of galactic monsters

# Galactic Monsters is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Galactic Monsters is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Player
class_name Enemy

enum ENEMY_MOVE_MODE {IDLE=0, LEFT=-1, RIGHT=1}

export (ENEMY_MOVE_MODE) var move_mode := ENEMY_MOVE_MODE.IDLE


func _ready() -> void:
	set_process_unhandled_input(false)


func _did_just_jump() -> bool:
	return false


func _get_input_direction() -> int:
	return move_mode


func _die() -> void:
	dead = true
	$AudioStreamPlayer2D2.play()
	sprite.play("hit")
	for _i in range(2):
		yield(sprite, "animation_finished")
	emit_signal("died")
	queue_free()
