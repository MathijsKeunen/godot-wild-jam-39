# Copyright (C) 2021  Mathijs Keunen

# This file is part of <program>

# <Program> is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# <Program> is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

signal all_enemies_died

var enemy_count: int


func _ready() -> void:
	var enemies := get_tree().get_nodes_in_group("enemy")
	enemy_count = enemies.size()
	for e in enemies:
		var enemy := e as Enemy
		if enemy:
# warning-ignore:return_value_discarded
			enemy.connect("died", self, "_on_monster_died")


func _on_monster_died() -> void:
	enemy_count -= 1
	if enemy_count == 0:
		emit_signal("all_enemies_died")


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("invert"):
		get_tree().call_group("gravity area", "invert_gravity")
