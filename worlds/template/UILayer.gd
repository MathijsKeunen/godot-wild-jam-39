# Copyright (C) 2021  Mathijs Keunen

# This file is part of <program>

# <Program> is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# <Program> is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

extends CanvasLayer

var paused := false

enum PAUSE_REASON {DIE, WIN}


func pause(reason: int) -> void:
	paused = not paused
	get_tree().paused = paused
	$CenterContainer.visible = paused
	if paused:
		if reason == PAUSE_REASON.DIE:
			$CenterContainer/VBoxContainer/die_label.visible = true
			$CenterContainer/VBoxContainer/win_label.visible = false
		elif reason == PAUSE_REASON.WIN:
			$CenterContainer/VBoxContainer/die_label.visible = false
			$CenterContainer/VBoxContainer/win_label.visible = true


func _on_TextureButton_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://worlds/world1.tscn")
	get_tree().paused = false


func _on_player_died() -> void:
	pause(PAUSE_REASON.DIE)


func _on_World_all_enemies_died() -> void:
	pause(PAUSE_REASON.WIN)
